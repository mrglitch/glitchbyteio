if [[ "$OSTYPE" == "linux-gnu" ]]; then
    sudo apt update -y && sudo apt upgrade -y
elif [[ "$OSTYPE" == "darwin" ]]; then
    brew update -y && brew upgrade -y
fi
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | zsh
nvm install --lts
node --version && npm --version
#git submodule add https://github.com/kaiiiz/hugo-theme-monochrome.git themes/monochrome
#hugo mod npm pack
#npm install
#echo "[build]\nwriteStats = true" >> config.toml