docker login registry.gitlab.com

# --platform needs to be specified if on an M1 mac. This builds the container
# and gets it ready to be pushed into the gitlab container registry
docker build --platform= linux/x86_64 -t registry.gitlab.com/mrglitch/glitchbyteio .

docker push registry.gitlab.com/mrglitch/glitchbyteio