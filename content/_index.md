---
title: Homepage
lastmod: 2022-11-15T18:18:30.819Z
---
{{< section >}}

This is the website of Glitchbyte. This site serves as an archive for my newsletter writings, First Principles. In my newsletter, I write about Computer Science concepts through game development, philosophy, and living a sustainable lifestyle. All concepts are broken down as simply as possible, not simpler.


{{< section "end" >}}

{{< newsletter >}}