---
title: Why programming is hard and what you can do about it
date: 2022-07-07T11:33:46-04:00
lastmod: 2022-07-09T02:16:53.704Z
draft: false
toc: true
images: null
description: Programming is for those of us who like to bang our heads against a problem
  until we solve it, but what makes it so hard?
categories:
  - programming
tags:
  - languages
  - history
  - computing
aliases: null
cssclass: null
keywords:
  - programming
slug: programming-hard
---


<div class="container">
  <img class="img-fluid" src="https://source.unsplash.com/fzOITuS1DIQ">
</div>

## A bit of programming history

In 1822, an inventor named Charles Babbage set out to create a device that could compute the values of polynomial functions by using the method of finite differences. Babbage received the funding needed to complete the project, but it was never completed. 

After prototyping the Difference Engine (DE), Babbage set out to create the “Analytical Engine” (AE), a more complex design than the DE. He completed this invention, building it so it could be programmed using punched cards to control a mechanical calculator.
 
One of the first recorded instances of programming occurred by Ada Lovelace on the AE. As a mathematician, she designed an algorithm that calculated the sequence of Bernoulli numbers. She is often referred to as the first computer programmer.

Alan Turing is another historical figure whose contributions to computing are seminal. As a mathematician, his primary job was to crack Enigma Machine codes during WWII. 

Turing later devised a computational device, the Electronic Numerical Integrator and Computer (ENIAC), to calculate artillery firing tables for the U.S. Army. 

Although it was completed after the war, ENIAC’s first task was to calculate the feasibility of a hydrogen bomb design. ENIAC was the first programmable general-purpose electronic digital computer.

At the time, most programmers were writing hand-tuned assembly programs. That would change with the advent of high-level programming languages like FORTRAN and FLOW-MATIC.

## The Programming Language
You have to learn the language and all its nuances.

Nowadays, we have programming languages such as C, Python, and Rust. While each language builds off of basic logic such as control flow, booleans, and functions, their implementations are different. For example, writing the famous first program “Hello World!”

In C, you must include a header file for input/output functions, declare `int main()`, print to the screen using`printf`, add a `;` at the end, and make sure you `return 0;` so the program knows you’re done.
```c
// Simple C program to display "Hello World!"
  
// Header file for input output functions
#include <stdio.h>
  
// main function -
// where the execution of program begins
int main()
{
  
    // prints hello world
    printf("Hello World!");
  
    return 0;
}
```

In Python, it’s as simple as using the builtin `print` function and passing your `"Hello World!"` string as an argument.
```python
# Simple Python program to display "Hello World!"

print("Hello World!")
```
These differences make it difficult to pick up other languages. You have to learn how each language implements:
- variables
- branching
- looping
- control flow
- functions

That’s just to name a few. Some languages need you to define data types as you code, others inference what data types you meant. Some languages have a compiler, others have an interpreter with garbage collection. 

Understanding all these differences allow you to make informed decisions when deciding to write software. It may not matter if you developed your 2D game in Python, but a 3D MMO may need the speed of C++. 
 
## The Programming Ecosystem
A language is hardly useful without its ecosystem. Which tools do you use and how do you choose them? Git, terminal, Linux, Windows, Powershell.

Choosing the right language to solve your problem is only part of the problem. Another part is the ecosystem built around the problem you’re trying to solve. 

Javascript is the de facto language for web applications. To build a basic website, you just need some HTML, CSS, and Javascript. 

You also need a server to deploy it. Yet, to build more complex web applications, you have to decide on a front-end framework:
- React
- Vue
- Svelte

A backend framework:
- Nodejs
- Nextjs
  
A program that builds your application:
- Webpack

That’s just to name a few examples. Nowadays, Typescript, a superset of Javascript that adds static typing to your code, is widely used. The ecosystem extends beyond the tooling you’d have to learn, too. 

A google search can reveal how many people have attempted to solve this problem before, how many guides are available, and who can help you if you get stuck. It extends to the community around the problem you're trying to solve.

That’s not to mention, you’ll have to have a base knowledge in version control (git), Linux/Powershell command line, debugging/profiling, etc.

## Programming within standards
Okay, you've chosen language to solve your problem, and you're comfortable in the ecosystem. You're ready to go, right?

Nope.

Most problems have a set of standards to follow, especially if you're developing a solution to a problem you’re intending to be public. 

Your web application needs to consider security standards such as the OWASP Web App security checklist, OWASP Top 10 Security Risks list, SANS Top 25 Most Dangerous Software errors, as well as best practices.

Do you intend on collaborating with others to complete your solution? You’ll need to adhere to coding conventions so everyone is on the same page, such as:
- Code formatting
- Documentation
- Application architecture
- Agreed upon tech stack

## Programming is all about problem-solving
Language understood? Check!
Ecosystem understood? Check!
Standards understood? Check!

All systems go?

Not yet.

Computers are complex devices historically designed by mathematicians to solve more and more complex problems
 
On top of knowing the above, they also need to know how to solve problems abstractly. To learn this, programmers learn data structures and algorithms, math, computer architecture, etc. 

Programming is an exercise of problem-solving, particularly solving puzzles with constraints. Those constraints are what a computer can realistically do. 

One of the most difficult things to grasp as a programmer is to think computationally, or as a computer would think. 

Computers are _really_ dumb. They can’t think for themselves, and they only understand on/off switches, or 1’s and 0’s. Through clever manipulation of on/off switches, we can make computers do amazing things!

That’s where practicing problems on sites like [Leetcode](https://leetcode.com/) and [HackerRank](https://www.hackerrank.com/) come in. These are websites that give you problems, let you practice them, and work through how to solve them. 

Websites like LeetCode and HackerRank help with computational thinking, but that’s only a part of the whole picture.

Solving real-world problems is another way to practice your computational thinking, by taking it and thinking abstractly for a computer to solve.

## The road to programming

Programming seems to be a pretty daunting task to undertake. With everything you have to learn, where do you even start?

Contrary to popular belief, don't worry about which language you start with so much. Analysis paralysis is a real thing, and while it may feel productive to do research on end, its just another form of procrastination if nothing comes from your research.

Also contrary to popular belief, picking up a strong, statically typed programming language. Rust is a good candidate for this, since you have to understand data types, memory management, and the ecosystem is the most loved by developers according to the Stack Overflow Survey 2022. 

On the opposite end, [I wouldnt recommend Python as a first programming language](https://glitchbyte.io/articles/why_python_is_not_the_best_programming_language_for_beginners/).

Regardless, the language doesnt matter nearly as much as the concepts you use it for. You'll want a solid grasp on the basics:
- Printing "Hello World!" to screen
- Variables and what "assignment" is
- Control flow (if/elif/else/finally)
- What "data types" are (string/int)
- How looping works (for/while)
- How data is arranged (arrays/lists/dictionaries/hashmaps)
- How data is retrieved (iteration/key value pairs)
- How functions work, what "variable scope" is, and how they get passed from function to function

From there, you can learn more advanced concepts such as:
- Classes and Object Oriented Programming
- Other programming paradigms such as functional and imperative
- Recursion
- Data structures and algorithms
- Memory management
- Fetching data from APIs


## The long road ahead
Programming is a difficult and long journey to undertake. There is a lot to learn, and the material can be hard to grasp, but the accomplishment is rewarding.

Programming is for those of us who like to bang our heads against a problem until we solve it. When we solve a problem, we feel a rush, and we are always seeking the next challenge.

You have to like puzzles. There’s no right way to solve each problem. It’s up to the programmer to decide the approach to take and where to go from here.

If you’re able to stick with it, programming is one of the most fun career tracks you can follow, which also happens to be lucrative. The reality is, that you do this kind of hard work because you enjoy it, not just for the money.

You constantly need to keep up with new trends, tools, techniques, languages, etc., to live on the cutting edge and always be willing to learn, in order to thrive.

Programming is hard, but that’s also why a lot of us enjoy it.