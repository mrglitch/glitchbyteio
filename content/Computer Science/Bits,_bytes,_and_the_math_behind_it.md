---
title: "The Machine Level"
date: 2023-09-11T:00:00-05:00
draft: true
# publishDate: 2023-09-13T11:00:00-05:00 
---


## Abstract

{{< epigraph >}}"There's a satisfaction in understanding how they (computers) work, knowing things very deeply. There's also a great confidence you can have when you know you could build something from scratch, even if you choose not to." - John Carmack{{< /epigraph >}}

Computers are basic machines at their core. They're a series of circuits fundamentally made up of transistors, little switches that either turn electricity on or off. This is no different than light switch in your house. 

Whether its on or off can be called a *state*, so we would ask what *state* is the switch in? On, or off?

Now think of all the light switches in your house. Each is in a room or section of the house. They each have a purpose. 

For example, my kitchen has two light switches. They both control the same light. If both of them are in an ON state, itll turn the light on.

If both of them are in an OFF state, itll also turn on the light. However, if they both have different states, the light will stay off. What if we look at the big picture though?

Take the whole house now. Say you have 40 light switches in the house. How many combinations can you put them in?

Quite a bit! In my kitchen alone, there are 4 combinations that result in 2 states, ON or OFF. 

Transistors are organized in a way that allow for many combinations to be used. With these combinations, we can build patterns for desired states. With those desired states, we can abstract them further and further, until we have what we do today. 

Technology now seems like magic, but only because transistors are 7nm (nanometers) small. For reference, a single human red blood cell is roughly 7,000nm.

That allows chip designers to put billions of transistors on a CPU. 

## Bits and Bytes

Since computers are made up of transistors and only have ON/OFF states, they can only count with either a 0 or a 1. 

For counting, we use the decimal system, otherwise known as Base10. All our numbers are made using a combination of 0-9.

However, because computers only have 1s and 0s, they use a different system called Base 2. 

We can count using base two using different patterns using these states. These states in machine language are called binary.

4-bits is known as a *nibble*, and can store 16 values.

```
0000 to 1111
```

8-bits is known as a byte, and can store 256 values.

An example:

```
Binary    | Decimal
--------- |--------
0000 0000 | 0
0000 0001 | 1
0000 0010 | 2
0000 0011 | 3
0000 0100 | 4
0000 0101 | 5
0000 0110 | 6
0000 0111 | 7
...       | ...
1111 1100 | 252
1111 1101 | 253
1111 1110 | 254
1111 1111 | 255
```

To count in binary, we start at the *least significant bit*, which is the farthest right bit. 

Depending on the architecture, bytes are stored in certain orders in memory addresses.

Computers have memory organized as a sequence of bytes. Each byte has an address. For multi-byte data types, such as a 32-bit integer, the storage spans multiple bytes. 

The order in which these bytes are stored in memory addresses can differ, and that's where endianness comes into play.

*Endianess* is the order in which bytes are stored in memory addresses. There are two primary endian formats, Little and Big.

### Little-Endian
The least significant byte (LSB) is stored at smallest memory address, and each subsequent byte is stored at the next higher address.

Common systems that use Little-Endian:
- x86 and x86-64 architectures
- ARM
- Most modern GPUs
### Big-Endian
The most significant byte (MSB) is stored at the smallest memory address, and each subsequent byte is stored at the next higher address. 

This is sometimes referred to as "network order" because it's the standard used in the Internet Protocol.

Common systems that use Big-Endian:
- Networking protocols
- MIPS-based systems
- Java Virtual machine (JVM)

## Hexadecimal

We can use another system to count, which uses a Base 16 system, hexadecimal.

Each architecture has a different amount of bytes. 
One hex digit can represent 4 binary digits.
```
8-bit:  
Binary: 0000 0000  Hex: 00 // 0
Binary: 0000 0001  Hex: 01 // 1

16-bit: 
Binary: 0000 0000 0000 0000  Hex: 0000 // 0
Binary: 0000 0000 0000 0001  Hex: 0001 // 1

24-bit: 
Binary: 0000 0000 0000 0000 0000 0000  Hex: 000000 // 0
Binary: 0000 0000 0000 0000 0000 0001  Hex: 000001 // 1

32-bit: 
Binary: 0000 0000 0000 0000 0000 0000 0000 0000  Hex: 0000 0000 // 0
Binary: 0000 0000 0000 0000 0000 0000 0000 0001  Hex: 0000 0001 // 1

64-bit:
...

```

```
Binary   | Decimal | Hexadecimal
--------|---------|-----------
0000    | 0       | 0
0001    | 1       | 1
0010    | 2       | 2
0011    | 3       | 3
0100    | 4       | 4
0101    | 5       | 5
0110    | 6       | 6
0111    | 7       | 7
1000    | 8       | 8
1001    | 9       | 9
1010    | 10      | A
1011    | 11      | B
1100    | 12      | C
1101    | 13      | D
1110    | 14      | E
1111    | 15      | F

```


| Bit Width | Commonly Used? | Context/Examples                                                       |
|-----------|----------------|------------------------------------------------------------------------|
| 8-bit     | Yes            | Early microprocessors like Intel 8080, Motorola 6800; many microcontrollers. |
| 16-bit    | Yes            | Intel 8086, Intel 80286; early Windows OS versions.                      |
| 24-bit    | Rarely         | Some DSPs, older systems; not common in general-purpose computers.       |
| 32-bit    | Yes            | Intel Pentium series, AMD Athlon, ARM Cortex-A; many OSs like early Windows, Linux. |
| 40-bit    | No             | Specific use cases, some DSPs; not for general computing.                |
| 48-bit    | No             | Early mainframe computers.                                               |
| 56-bit    | No             | Specific to encryption (like DES's 56-bit keys), not a computer architecture. |
| 64-bit    | Yes            | Modern CPUs: Intel Core, AMD Ryzen; modern OSs: Windows 10 64-bit, macOS, Linux.  |


## ASCII

ASCII (American Standard Code for Information Interchange) is a character encoding standard used for representing text and control characters in computers, communication equipment, and other devices that use text. 

ASCII uses a 7-bit binary number to represent each character, although it's often stored in 8-bit bytes in modern systems. 

The first 32 codes (0-31) are control codes, and the next set of codes (32-127) represent printable characters.

```
Dec | Hex | Char      Dec | Hex | Char      Dec | Hex | Char      Dec | Hex | Char
----|-----|-----      ----|-----|-----      ----|-----|-----      ----|-----|----
  0 | 00  | NUL       32  | 20  | SPACE     64  | 40  | @         96  | 60  | `
  1 | 01  | SOH       33  | 21  | !         65  | 41  | A         97  | 61  | a
  2 | 02  | STX       34  | 22  | "         66  | 42  | B         98  | 62  | b
  3 | 03  | ETX       35  | 23  | #         67  | 43  | C         99  | 63  | c
  4 | 04  | EOT       36  | 24  | $         68  | 44  | D        100  | 64  | d
  5 | 05  | ENQ       37  | 25  | %         69  | 45  | E        101  | 65  | e
  6 | 06  | ACK       38  | 26  | &         70  | 46  | F        102  | 66  | f
  7 | 07  | BEL       39  | 27  | '         71  | 47  | G        103  | 67  | g
  8 | 08  | BS        40  | 28  | (         72  | 48  | H        104  | 68  | h
  9 | 09  | TAB       41  | 29  | )         73  | 49  | I        105  | 69  | i
 10 | 0A  | LF        42  | 2A  | *         74  | 4A  | J        106  | 6A  | j
 11 | 0B  | VT        43  | 2B  | +         75  | 4B  | K        107  | 6B  | k
 12 | 0C  | FF        44  | 2C  | ,         76  | 4C  | L        108  | 6C  | l
 13 | 0D  | CR        45  | 2D  | -         77  | 4D  | M        109  | 6D  | m
 14 | 0E  | SO        46  | 2E  | .         78  | 4E  | N        110  | 6E  | n
 15 | 0F  | SI        47  | 2F  | /         79  | 4F  | O        111  | 6F  | o
 16 | 10  | DLE       48  | 30  | 0         80  | 50  | P        112  | 70  | p
 17 | 11  | DC1       49  | 31  | 1         81  | 51  | Q        113  | 71  | q
 18 | 12  | DC2       50  | 32  | 2         82  | 52  | R        114  | 72  | r
 19 | 13  | DC3       51  | 33  | 3         83  | 53  | S        115  | 73  | s
 20 | 14  | DC4       52  | 34  | 4         84  | 54  | T        116  | 74  | t
 21 | 15  | NAK       53  | 35  | 5         85  | 55  | U        117  | 75  | u
 22 | 16  | SYN       54  | 36  | 6         86  | 56  | V        118  | 76  | v
 23 | 17  | ETB       55  | 37  | 7         87  | 57  | W        119  | 77  | w
 24 | 18  | CAN       56  | 38  | 8         88  | 58  | X        120  | 78  | x
 25 | 19  | EM        57  | 39  | 9         89  | 59  | Y        121  | 79  | y
 26 | 1A  | SUB       58  | 3A  | :         90  | 5A  | Z        122  | 7A  | z
 27 | 1B  | ESC       59  | 3B  | ;         91  | 5B  | [        123  | 7B  | {   28 | 1C  | FS        60  | 3C  | <         92  | 5C  | \        124  | 7C  | |   29 | 1D  | GS        61  | 3D  | =         93  | 5D  | ]        125  | 7D  | }
 30 | 1E  | RS        62  | 3E  | >         94  | 5E  | ^        126  | 7E  | ~
 31 | 1F  | US        63  | 3F  | ?         95  | 5F  | _        127  | 7F  | DEL
```

## A programs memory layout

```
Address           Segment/Section           Description 
--------------------------------------------------------------
0x40000000   |    Text/Code (.text)      | // Exectuable code.
             |                           | 
0x40020000   +---------------------------|--------------------
             |   Initialized Data (.data)| // Initialize global/static
             |                           | // variables.
0x40021000   +---------------------------|--------------------
             | Uninitialized Data (.bss) | // Uninit global/static variables.
             |                           | // set to zero by system.
0x40021500   +---------------------------|--------------------
             |         Heap              | // Dynamic Memory. Starts empty.
             |                           | // Grows upwards.
0x40050000   +---------------------------|--------------------
             |        (Gap/Unused)       | // May be reserved for heap.
             |                           |
0x7FFFE000   +---------------------------|--------------------
             |         Stack             | // Function call data.
             |                           | // Grows downwards.
0x80000000   +---------------------------|--------------------

```

## See Also

## External Links

## Footnotes

## Further Reading

## References
