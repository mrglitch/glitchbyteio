---
title: Why Python isn't the best programming language for beginners
date: today
lastmod: 2022-11-12T04:08:03.333Z
draft: false
toc: true
images: null
description: null
categories:
  - Computer Science
tags: null
aliases: null
cssclass: null
keywords: null
slug: null
type: null
---

{{< section >}}

You've probably heard it a ton of times before; if you want to learn how to program, Python is the best language to start with. It's simple to pick up, the language is forgiving of mistakes, and there's a library for everything. Python almost reads like plain english too!


However, this is terrible advice. To understand why, though, we need to understand what Python is.
Before I begin, I want to be clear: Python is a fantastic programming language. There are many good reasons why Python is so popular and why its so often recommended to people who want to begin coding. Python allows for rapid prototyping, can be used to easily create scripts, and seems to be the language of choice in the data science space. Python makes a lot of things easy.{{< sidenote >}} Some Side note goes here {{</ sidenote >}}


The issue is specifically telling brand new programmers that Python is the language to learn. 

{{<section "end" >}}

{{< section >}}
## What is Python?

{{< blockquote cite="www.shawnohare.com" footer="Shawn" >}}
  There is nothing more beautiful than an elegant mathematical proof.
{{< /blockquote >}}

Besides the obvious "it's a programming language", Python is a dynamically-typed, interpreted language written in C/C++ by Guido van Rossum and released in 1991.

Van Rossum began working on Python as a hobby language to succeed the ABC programming language, a language he had worked on before. Stemmed from his frustrations with the [ABC programming language](https://en.wikipedia.org/wiki/ABC_(programming_language)), Python is a small core language with a massive standard library.

Over the years, Python has become a multi-paradigm language, incorporating the object-oriented, structured, functional, and aspect-oriented paradigms. Many other unmentioned paradigms are supported via extensions.

Python uses dynamic typing and a garbage collector. This is where the problems begin.
{{<section "end" >}}

{{< section >}}
## Garbage Collector

Garbage collection is a form of automatic memory management invented by John McCarthy around 1959 to simplify memory management in LISP.

The garbage collector attempts to reclaim memory allocated by the program that is no longer referenced.

Python uses reference counting garbage collection where each object has a count of the number of references to it. This guarantees that when the last reference is destroyed, the object itself is destroyed, freeing up the memory it took.
{{<section "end" >}}

## Dynamically typed

Variables can just be assigned. Garbage collector keeps track of what's what.

## Variables change

Variables are _always_ mutable.

## What is your code doing?

What the heck is "for" doing anyway?

## C is better, but not perfect

If you know C, you know programming; if you know Python, you know Python. Its abstracted further away from programming. 

Memory management makes it difficult.

## Rust seems to be a great middle ground

Rust gives you the power of C and a syntax/dev experience similar to Python.

[^1]: This is an example.
