---
title: Combinations vs. Permutations
date: 2022-11-13T12:41:31-05:00
lastmod: 2022-11-15T18:25:37.654Z
draft: true
toc: true
images: null
description: null
categories: null
tags: null
aliases: null
keywords: null
slug: null
type: null
---

## Abstract

{{< epigraph >}}{{< /epigraph >}}

## Content

% \f is defined as #1f(#2) using the macro
\f\relax{x} = \int_{-\infty}^\infty
    \f\hat\xi\,e^{2 \pi i \xi x}
    \,d\xi


## See Also

## External Links

## Footnotes

## Further Reading

## References
