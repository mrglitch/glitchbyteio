---
title: Rust + Tauri setup on Windows
date: 2022-07-06T14:30:17-04:00
lastmod: 2022-07-09T02:08:56.327Z
draft: false
toc: true
images: null
categories:
  - Rust
tags:
  - rust
  - programming
  - tauri
aliases: null
cssclass: null
---

# Rust + Tauri setup on Windows


## Setup WSL
1. Open PowerShell and enter the following:
  `wsl --install`
2. Restart you computer
3. Open up a Powershell window and type in `wsl`.
4. Configure your username and password.
5. The default linux distro installed is Ubuntu. To update: 
  `sudo apt update -y && sudo apt upgrade -y`

## Setup VSCode with WSL
1. Open VSCode.
2. Click the 'Extensions' icon (4 boxes) on the left.
3. Search for 'Remote WSL'.
4. Install 'Remote WSL'. It should detect your WSL instance.
5. Add CA Certificate for SSL verification through WSL.
   `sudo apt-get install wget curl ca-certificates`

## Install Git (Version Control)
1. In VSCode, use `ctrl+shift+p` to open up a new WSL terminal if one is not up already.
2. Type in `sudo apt install git`
3. Look for the Git VSCode extension; install it.

## Install Rust
1. Open a new terminal.
2. Paste and enter the following command; this will download and install Rust:
   `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
3. Make sure it installed correctly using `rustc --version`. If you get a version number, it's installed.

## Install NPM for Javascript portion of app
1. Install NVM (Node Version Manager): `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash`
2. Verify installation: `nvm -v` or `command -v nvm`
3. Install NPM: `nvm install --lts`
   - This install the latest stable release of NPM.
4. Install Nodejs, which is needed by NPM: `nvm install node`
5. Verify installation: `nvm ls`
   - You should see version numbers instead of `N/A`
6. Search and install the `Node Extension Pack` and `Javascript Debugger` in VSCode's extensions. 

## Get Tauri up and running
1. Install system dependencies for Tauri:
```bash
sudo apt update
sudo apt install libwebkit2gtk-4.0-dev \
    build-essential \
    curl \
    wget \
    libssl-dev \
    libgtk-3-dev \
    libayatana-appindicator3-dev \
    librsvg2-dev
```
2. Install the Tauri CLI:
  `cargo install tauri-cli --version "^1.0.0"`
3. Verify Tauri CLI installation:
  `cargo tauri --help`
4. Start a new Tauri app: `npm create tauri-app`
5. Run through prompts given.