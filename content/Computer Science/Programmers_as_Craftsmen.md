---
title: Programmers as Craftsmen
date: 2023-10-03T20:49:38-04:00
lastmod: 2022-09-16T18:35:26.157Z
draft: false
toc: true
images: null
description: Programmers are akin to painters.
categories: ["programming", "art", "c"]
aliases: null
keywords: null
slug: null
type: null
meta: true
---

{{<epigraph>}}
"If you wish to make an apple pie, you must first invent the universe." - Carl Sagan
{{</epigraph>}}


{{<section>}}
## The Painter

Painters spend their whole life on the their craft. They learn shapes, line weight, value, hues, anatomy.  The list goes on.

Many of the painters we revere create pieces of majesty. Davinci, Picasso, Michelangelo, are but a few names we recognize.

In painting, you learn how to see the world differently. The mind has a tendency to see an object and make a symbol for it, archiving it later for future use. 

When you draw that object, your mind pulls from that archive and attempts to draw the symbol, rather than from the object itself.

This is why painters learn to see with shapes and value. Everything breaks down into the simplest of shapes, and a few tones of color.
Many painters will start by cultivating those two skills first before moving onto more advanced techniques.

In painting, theres not one road to success. The only common thread is consistent practice.

Painters choose their own paths, exploring what the art world has to offer.

Over the years, a painter will develop an intuition on what works, and what doesn't. This comes from two things:

- A strong foundation in the fundamentals.
- The curiosity to explore and push the bounds of those fundamentals.

Nobody can teach the painter their intuition. Intuition is not a step by step process.

It is a process developed over time, guided by peers, studying inspirations, and hammering away at those fundamentals.
{{<section "end">}}

{{<section>}}
## The Programmer

Programmers spend years on their craft. They learn from books, videos, different languages, reading code, writing code. The list goes on.

Many of the greatest programs are works of art. Linux changed the world of operating systems; DOOM changing the world of first person shooters and what it means to write a game.

Dennis Ritchie, Linus Torvalds, John Carmack, Donald Knuth, Ken Thompson. The list can go on and on, but the thread remains common: these are computer wizards, true masters of their art. They are the ones the rest of us aspire to be.

In programming, you learn to see the world differently. You begin to see the logic of reality. How different states represent other states. How we abstract everything around us.

Take a house, for example.

We do not describe a house based on its parts. We don't say the wood makes up the frame, the electrical wiring is the best money can offer, or the plumbing unparalleled in reliability. 

We describe a house based on its function. It has X amount of rooms with Y bathrooms, perfect for a family of size Z. 

As programmers, we know theres no one road to success. We can take any number of paths we desire. Not all will lead to us becoming the artists we truly want to become, though.

The greatest programmers of our time had nothing to work with. No frameworks, no guides online, nothing but their wit, grit, and a few books here and there.

So they invented a path forward to make it easier on the next generation. Along the way, we forgot to study how the masters became the masters.

Before we can paint our own masterpieces, we must learn as the masters did. To see the shapes, values, hue, anatomy.

To see with our eyes, not our minds. Not what we think it should be, but rather what it is.

Frameworks, new languages, new paradigms, are always changing and in flux. Yet, the basis of a computer has remained the same for decades.

Computers are machines made of many little switches that create different states. A bit of an oversimplification, but true.

Computer architectures has hardly changed. Von Neumann's model is still the de facto standard used today.

We need to go back to the fundamentals.
{{<section "end">}}

{{<section>}}
## The Craftsman

> What do you mean by fundamentals?

Imagine someone wants to learn how to paint. We tell them the best way to start is by going to your nearest book store, picking up a coloring book, and color to their hearts content.

The rationale? 

A coloring book already has the lines drawn for you, the picture laid out, so all you have to do is fill in the blanks.

Ridiculous, right?

There's nothing wrong with this advice. They can paint within the lines given to them by the coloring book, but it's not what they had in mind.
Instead, they were thinking of something like an easel, a blank canvas, paints, and brushes.

The problem with the blank canvas, though, is a daunting one. There is no frame of reference to start. Anything goes with just a few brushes and paints.

How does a painter start with a blank canvas, then? It has a lot to do with those fundamentals mentioned earlier. They know certain objects break down to the most basic shapes, and a few colors.

From there, they can add all the detail they choose, in whatever style they desire.
Unfortunately, in programming, we give newcomers the coloring book instead of the blank canvas. 

We recommend tools like Python or Javascript, that abstract much of the useful fundamentals away from the users.

Then they move onto frameworks.

These aren't fundamentals. These are building blocks built upon the shoulders of giants for _years_. 

Fundamentals is understanding the data structures and algorithms you're using, why you're using them, and when to pick the right one.

Fundamentals is understanding Object Oriented Programming isn't the best paradigm out there for every job.

Fundamentals is understanding how programs get represented in memory, and how that affects your program.

We can only learn these things if we get our hands dirty and build them from scratch.

When we start to build technologies from scratch, we begin to notice similar patterns from two different problems. We can distill those problems to most basic shape and build from there.

This has been my experience. I started with Python over 5 years ago. I was able to write web scrapers and throw together an application if I had to, but it wasn't without significant effort.

It wasn't until I learned C that everything started to click. I understood why control flow worked as it did. I understood how memory is manipulated. I understood there's a difference between data and operations done on that data. 

My experience with C was enlightening, but not as deep as I would have liked.

I learned how to start a codebase, create some data structures and algorithms, and write small programs. I never moved beyond that.

From C, I moved onto Rust, and have been coding in that for almost two years. I can proudly say I wrote a significant application in Rust for work, and I teach my colleagues the Rust language.

Yet, I still love C, and I still have a lot to learn from it.

> That's a difficult language to program in!

That's the point. The language itself is not complex, but you have to write everything yourself. You need to know what shapes to form and which colors to use.

In the same way a painter can make a masterpiece or a mess, so can one in C.

Its only with proper study and consistent practice that the novice painter becomes a master.

> Where do we even start?

That's always been the hard question. There's no one right answer, but I can at least share mine.

First, we need to get familiar with our tools.

I'll start with:

- C syntax
- Basic program structure/running a program
- Memory and pointers
- Starting a codebase

From there, the canvas is blank, and up to us on where we want to take it.
{{<section "end">}}