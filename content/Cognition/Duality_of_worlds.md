---
title: Duality_of_worlds
date: 2022-06-23T01:28:44.000Z
lastmod: 2022-09-23T15:42:16.747Z
draft: true
toc: true
images: null
categories:
  - nocat
tags:
  - untagged
aliases: null
cssclass: null
---

<https://ia803404.us.archive.org/8/items/as-we-may-think/As%20We%20May%20Think.pdf>

## Discovering the digital world

## The digital frontier

## Sinister natives

computers make us dumber, not smarter?
Reliance on technology?
Confirmation of cognitive biases?

## Living in this strange land

Are we master, or slave?

## Time to go home

What can we do about it?
